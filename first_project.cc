#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(1280, 720), "SFML works!");
	window.setFramerateLimit(60);
	sf::CircleShape shape(10.f);
	//sf::RectangleShape shape(sf::Vector2f(100, 100));
	
	shape.setOutlineThickness(10);
	shape.setOutlineColor(sf::Color::Yellow);
	shape.setFillColor(sf::Color::Green);
	shape.setPosition(10, 10);
	
	sf::Event event;

	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)	
				window.close();
		}
		
		shape.move(1, 1);

		window.clear();
		
		window.draw(shape);
		
		window.display();
	}

	return 0;
}
