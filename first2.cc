#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>

// Ball with velocity vector (int array) in rectangular form
class MyBall: public sf::CircleShape{
	public:
		MyBall(float, unsigned int, float, float);
		float vel[2]={0, 0};
};

MyBall::MyBall(float radius = 0.0, unsigned int pointCount = 30, float xvel = 0, float yvel = 0)
	: sf::CircleShape(radius, pointCount)
{
	vel[0] = xvel;
	vel[1] = yvel;
}

// Typedefs
typedef std::vector<MyBall> Ball_V;

// Functions (Forward Declared)
void gen_ball(Ball_V &);
void shockwave(Ball_V &, int, int, short);
void side_push(Ball_V &, short, short);
void scatter(Ball_V &);
void spin(Ball_V &);

void wub_wub(Ball_V &);
void dewub(Ball_V &);

void fade(sf::Uint8 &, sf::Uint8 &, sf::Uint8 &);
void fade(sf::Uint8*, const sf::Uint8 &);

// Global Variables
int SCREEN_WIDTH = 1280;
int SCREEN_HEIGHT = 944;
float wub_rate(0.05);
float cur_wub(1);
sf::Texture trumps[6];


int main()
{
	// Set up Pseudo-random number generator
	srand(time(NULL));
	
	// Load images
	for (int i(0); i < 6; ++i)
	{
		trumps[i].loadFromFile("/home/s-michael-liu/SFML/assets/t_circles/t" + std::to_string(i) + ".png");
	}
	
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "SFML window");
    window.setFramerateLimit(60);
    
    // Variables
    Ball_V circles;
    bool wub(false);
    bool dizzy_trump(false);
    sf::Uint8 colors[3] = {0, 128, 255};
    
    for (int i(0); i < 50; ++i)
    {
		gen_ball(circles);
	}
    
    sf::Event event;

	// Window loop
    while (window.isOpen())
    {
        // Process events
        while (window.pollEvent(event))
        {
            // Close window: exit
            switch (event.type)
            {
				case sf::Event::Closed: {window.close(); break;}
				case sf::Event::MouseButtonPressed:
				{
					switch (event.mouseButton.button)
					{
						case sf::Mouse::Right: {shockwave(circles, event.mouseButton.x, event.mouseButton.y, 1); break;}
						case sf::Mouse::Left: {shockwave(circles, event.mouseButton.x, event.mouseButton.y, -1); break;}
						default: {break;}
					}
					break;
				}
				case sf::Event::KeyPressed:
				{
					switch (event.key.code)
					{
						case sf::Keyboard::Escape: {window.close(); break;}
						case sf::Keyboard::A: {gen_ball(circles); break;}
						case sf::Keyboard::W: {wub = true; break;}
						case sf::Keyboard::S: {scatter(circles); break; }
						case sf::Keyboard::R: {dizzy_trump = true; break;}
						case sf::Keyboard::Up: {side_push(circles, 0, -1); break;}
						case sf::Keyboard::Down: {side_push(circles, 0, 1); break;}
						case sf::Keyboard::Left: {side_push(circles, -1, 0); break;}
						case sf::Keyboard::Right: {side_push(circles, 1, 0); break;}
						default: {break;}
					}
					break;
				}
				case sf::Event::KeyReleased:
				{
					switch (event.key.code)
					{
						case sf::Keyboard::W: {dewub(circles); wub = false; break;}
						case sf::Keyboard::R: {dizzy_trump = false;}
						default: break;
					}
					break;
				}
				
				default: {break;}
			}
        }
        // Clear screen
        //window.clear(sf::Color(rand() % 256, rand() % 256, rand() % 256));
        //fade(colors[0], colors[1], colors[2]);
        window.clear(sf::Color(colors[0], colors[1], colors[2]));
		
		if (wub) wub_wub(circles);
		
		if (dizzy_trump) spin(circles);
		
        for (auto &circle:circles)
        {
			static sf::Vector2f pos;
			pos.x = circle.getPosition().x;
			pos.y = circle.getPosition().y;
			
			if (pos.x <= circle.getRadius())
				{ circle.vel[0] *= -1; circle.setPosition(circle.getRadius(), pos.y); }
			else if (pos.x >= SCREEN_WIDTH - circle.getRadius())
				{ circle.vel[0] *= -1; circle.setPosition(SCREEN_WIDTH - circle.getRadius(), pos.y); }
			if (pos.y <= circle.getRadius())
				{ circle.vel[1] *= -1; circle.setPosition(pos.x, circle.getRadius()); }
			else if (pos.y >= SCREEN_HEIGHT - circle.getRadius())
				{ circle.vel[1] *= -1; circle.setPosition(pos.x, SCREEN_HEIGHT - circle.getRadius()); }
			
			circle.move(circle.vel[0], circle.vel[1]);
			
			circle.vel[0] *= 0.99;
			circle.vel[1] *= 0.99;
			
			window.draw(circle);
		}
        window.display();
    }
    return EXIT_SUCCESS;
}	

void gen_ball(Ball_V &circles){
	circles.push_back(MyBall(rand() % 51, 30, (rand() % 12) - 5, (rand() % 12) - 5));
	circles.back().setPosition(	rand() % (int)(SCREEN_WIDTH - circles.back().getRadius() * 2) + circles.back().getRadius(), 
								rand() % (int)(SCREEN_HEIGHT - circles.back().getRadius() * 2) + circles.back().getRadius());
	circles.back().setOrigin(circles.back().getRadius(), circles.back().getRadius());
	int r = rand() % 6;
	if (r < 6)
		circles.back().setTexture(&trumps[r], true);
	else
		circles.back().setFillColor(sf::Color(rand() % 256, rand() % 256, rand() % 256));
	return;
}

void shockwave(Ball_V &circles, int x, int y, short o){
	int dx, dy;
	double r, dd;
	for (auto &circle:circles)
	{
		dx = circle.getPosition().x - x;
		dy = circle.getPosition().y - y;
		dd = sqrt(pow(dx, 2) + pow(dy, 2));
		r = 100/pow(dd, 1.5);
		circle.vel[0] += r * dx * o;
		circle.vel[1] += r * dy * o;
	}
}

void side_push(Ball_V &circles, short x, short y){
	int dd;
	double r;
	for (auto &circle:circles)
	{
		if (x) dd = (x == 1)?circle.getPosition().x:SCREEN_WIDTH - circle.getPosition().x;
		if (y) dd = (y == 1)?circle.getPosition().y:SCREEN_HEIGHT - circle.getPosition().y;
		
		r = 1000 / pow(dd, 1.25);
		
		circle.vel[0] += r * x;
		circle.vel[1] += r * y; 
	}
}

void scatter(Ball_V &circles){
	for (auto &circle:circles)
	{
		circle.vel[0] = rand() % 11 - 5;
		circle.vel[1] = rand() % 11 - 5;
	}
}

void spin(Ball_V &circles){
	for (auto &circle:circles)
		circle.setRotation((circle.getRotation() + 2));
}

void wub_wub(Ball_V &circles){
	cur_wub += wub_rate;
	if (cur_wub >= 1.25 || cur_wub <= 0.75) wub_rate *= -1;
	for (auto &circle:circles)
		circle.setScale(cur_wub, cur_wub);
}

void dewub(Ball_V &circles){
	for (auto &circle:circles)
	{
		circle.setScale(1, 1);
		cur_wub = 1;
	}
}

void fade(sf::Uint8 &r, sf::Uint8 &g, sf::Uint8 &b){
	r = ( ++r ) % 256;
	g = ( ++g ) % 256;
	b = ( ++b ) % 256;
}
