#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>



// Ball with velocity vector (int array) in rectangular form
class MyBall: public sf::CircleShape{
	public:
		MyBall(float, unsigned int, float, float);
		float vel[2]={0, 0};
};

MyBall::MyBall(float radius = 0.0, unsigned int pointCount = 30, float xvel = 0, float yvel = 0)
	: sf::CircleShape(radius, pointCount)
{
	vel[0] = xvel;
	vel[1] = yvel;
}

// Functions (Forward Declared)
void gen_ball(std::vector<MyBall> &); // Creates a new ball with random size, position, color, and velocity
void shockwave(std::vector<MyBall> &, int, int); // Pushes all balls away from cursor position on right click
void blackhole(std::vector<MyBall> &, int, int); // Pulls all balls to cursor position on left click
void side_push(std::vector<MyBall> &, short, short); // Pushes all balls in the direction of button press
void dewub(std::vector<MyBall> &);

// Global Variables
int SCREEN_WIDTH = 1024;
int SCREEN_HEIGHT = 720;

int main()
{
	// Set up Pseudo-random number generator
	srand(time(NULL));
	
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "SFML window");
    window.setFramerateLimit(60);
    
    // Variables
    std::vector<MyBall> circles;
    bool wub_wub = false;
    
    for (int i(0); i < 20; ++i)
    {
		gen_ball(circles);
	}
    
	// Window loop
    while (window.isOpen())
    {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            switch (event.type)
            {
				case sf::Event::Closed: {window.close(); break;}
				case sf::Event::MouseButtonPressed:
				{
					switch (event.mouseButton.button)
					{
						case sf::Mouse::Right: {shockwave(circles, event.mouseButton.x, event.mouseButton.y); break;}
						case sf::Mouse::Left: {blackhole(circles, event.mouseButton.x, event.mouseButton.y); break;}
						default: {break;}
					}
					break;
				}
				case sf::Event::KeyPressed:
				{
					switch (event.key.code)
					{
						case sf::Keyboard::Escape: {window.close(); break;}
						case sf::Keyboard::A: {gen_ball(circles); break;}
						case sf::Keyboard::W: {wub_wub = true; break;}
						case sf::Keyboard::Up: {side_push(circles, 0, -1); break;}
						case sf::Keyboard::Down: {side_push(circles, 0, 1); break;}
						case sf::Keyboard::Left: {side_push(circles, -1, 0); break;}
						case sf::Keyboard::Right: {side_push(circles, 1, 0); break;}
						default: {break;}
					}
					break;
				}
				case sf::Event::KeyReleased:
				{
					switch (event.key.code)
					{
						case sf::Keyboard::W: {wub_wub = false; break;}
						default: {break;}
					}
					break;
				}
				default: {break;}
			}
        }
        // Clear screen
        window.clear(sf::Color(255, 0, 128));

        for (auto &circle:circles)
        {
			static sf::Vector2f pos;
			pos.x = circle.getPosition().x;
			pos.y = circle.getPosition().y;
			
			// Circle hits wall
			if (pos.x <= circle.getRadius()) 
				{ circle.vel[0] *= -1; circle.setPosition(circle.getRadius(), pos.y); }
			else if (pos.x >= SCREEN_WIDTH - circle.getRadius())
				{ circle.vel[0] *= -1; circle.setPosition(SCREEN_WIDTH - circle.getRadius(), pos.y); }
			if (pos.y <= circle.getRadius())
				{ circle.vel[1] *= -1; circle.setPosition(pos.x, circle.getRadius()); }
			else if (pos.y >= SCREEN_HEIGHT - circle.getRadius()) 
				{ circle.vel[1] *= -1; circle.setPosition(pos.x, SCREEN_HEIGHT - circle.getRadius()); }
				
			// WubWub
			if (wub_wub)
			{
				static float s;
				s = circle.getScale().x + 0.1;
				circle.setScale(s, s);
			}
			
			// Move the circle
			circle.move(circle.vel[0], circle.vel[1]);
			
			// Decelerate the circle
			circle.vel[0] *= 0.99;
			circle.vel[1] *= 0.99;
			
			// Draw the circle on the screen
			window.draw(circle);
		}
        window.display();
    }
    return EXIT_SUCCESS;
}	

void gen_ball(std::vector<MyBall> &circles){
	circles.push_back(MyBall(rand() % 51, 30, (rand() % 12) - 5, (rand() % 12) - 5));
	circles.back().setPosition(	rand() % (int)(SCREEN_WIDTH - circles.back().getRadius() * 2) + circles.back().getRadius(), 
								rand() % (int)(SCREEN_HEIGHT - circles.back().getRadius() * 2) + circles.back().getRadius());
	circles.back().setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255));
	circles.back().setOrigin(circles.back().getRadius(), circles.back().getRadius());
	return;
}

void shockwave(std::vector<MyBall> &circles, int x, int y){
	int dx, dy;
	double r, dd;
	for (auto &circle:circles)
	{
		dx = circle.getPosition().x - x;
		dy = circle.getPosition().y - y;
		dd = sqrt(pow(dx, 2) + pow(dy, 2));
		r = 100/pow(dd, 1.5);
		circle.vel[0] += r * dx;
		circle.vel[1] += r * dy;
	}
}

void blackhole(std::vector<MyBall> &circles, int x, int y){
	int dx, dy;
	double r, dd;
	for (auto &circle:circles)
	{
		dx = circle.getPosition().x - x;
		dy = circle.getPosition().y - y;
		dd = sqrt(pow(dx, 2) + pow(dy, 2));
		r = 100/pow(dd, 1.5);
		circle.vel[0] -= r * dx;
		circle.vel[1] -= r * dy;
	}
}

void side_push(std::vector<MyBall> &circles, short x, short y){
	int dd;
	double r;
	for (auto &circle:circles)
	{
		if (x) dd = (x == 1)?circle.getPosition().x:SCREEN_WIDTH - circle.getPosition().x;
		if (y) dd = (y == 1)?circle.getPosition().y:SCREEN_HEIGHT - circle.getPosition().y;
		
		r = 1000 / pow(dd, 1.25);
		
		circle.vel[0] += r * x;
		circle.vel[1] += r * y; 
	}
}

void dewub(vector<MyBall> &circles){
	return;
}
